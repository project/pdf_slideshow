<?php

namespace Drupal\pdf_slideshow\Manager;

use Drupal\Core\File\FileSystem;
use Imagick;

/**
 * Class PdfSlideshowImagickManager.
 *
 * @package Drupal\pdf_slideshow\Manager
 */
class PdfSlideshowImagickManager {

  protected $fileSystem;

  /**
   * PdfSlideshowImagickManager constructor.
   *
   * @param $fileSystem
   */
  public function __construct(FileSystem $fileSystem) {
    $this->fileSystem = $fileSystem;
  }


  /**
   * Generate images from PDF file.
   *
   * @param string $source
   *   Files source.
   * @param string $target
   *   Files target.
   * @param int $width
   *   Image width.
   * @param int $height
   *   Image height.
   * @param int $limit
   *   Tweet limit.
   *
   * @return array
   *   Files path.
   *
   * @throws \ImagickException
   */
  public function generateImage($source, $target, $width = 200, $height = 250, $limit = 3) {
    $target = dirname($source) . DIRECTORY_SEPARATOR . $target;
    $im = new Imagick($source);
    $im->setimageformat("png");
    $im->setImageAlphaChannel(Imagick::ALPHACHANNEL_REMOVE);
    $im->mergeImageLayers(Imagick::LAYERMETHOD_FLATTEN);
    $im->setImageBackgroundColor('white');
    $nbImage = $im->getNumberImages();
    $imageFilenames = [];
    for ($i = 0; $i < $nbImage && $i < $limit; $i++) {
      $im->setIteratorIndex($i);
      $im->thumbnailimage($width, $height);
      $im->writeImage(str_replace('.png', '-' . $i . '.png', $target));
      $imageFilenames[] = $im->getImageFilename();
    }
    $im->clear();
    $im->destroy();
    return $this->moveFilesToPublicDirectory($imageFilenames);
  }

  /**
   * Move files to public path.
   *
   * @param array $fileNames
   *   File names.
   *
   * @return array
   *   File path after move.
   */
  public function moveFilesToPublicDirectory(array $fileNames) {
    $newFilePath = [];
    foreach ($fileNames as $fileName) {
      if (file_exists($fileName)) {
        $filePathArray = explode('/', $fileName);
        if (!empty($filePathArray)) {
          $parentFolder = $filePathArray[count($filePathArray) - 2];
          if ($parentFolder) {
            $destination = 'public://pdf_slideshow/' . $parentFolder;
            if ($this->fileSystem->prepareDirectory($destination,
              FileSystem::CREATE_DIRECTORY)) {
              $this->fileSystem->move($fileName,
                $destination,
                FileSystem::EXISTS_REPLACE);
              $newFilePath[] = $destination . '/' . $filePathArray[count($filePathArray) - 1];
            }
          }
        }
      }
    }
    return $newFilePath;
  }
}
