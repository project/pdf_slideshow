INTRODUCTION
------------

The Last Tweets module displays the last posted tweet for a given account.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/pdf_slideshow

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/pdf_slideshow

REQUIREMENTS
------------

This module requires the following PHP extension: imagick.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
